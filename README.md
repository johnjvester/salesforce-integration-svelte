# salesforce-integration-svelte Repository

> The `salesforce-integration-svelte` repository is very basic [Svelte](https://svelte.dev) client application
> designed to make RESTful calls to the [`salesforce-integration-service`](https://gitlab.com/johnjvester/salesforce-integration-service) 
> repository, which interacts with a [Salesforce](http://www.salesforce.com) instance in order to retrieve (GET) and 
> update (PATCH) `Contact` objects.

## Publications

This repository is related to an article published on DZone.com:

* TBD

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Important Information

This is a very quick Svelte application, which expects the `salesforce-integration-service` to be running at the following 
URL:

```shell
http://localhost:9999/contacts
```

For now, if this value needs to change, simply update the following sections of code in the `Contacts.svelte` file:

```javascript
    onMount(async () => {
        await fetch(`http://localhost:9999/contacts`)
            .then(r => r.json())
            .then(data => {
                contacts = data;
            });
    });
```

```javascript
        const res = await fetch('http://localhost:9999/contacts/' + contact.id, {
            method: 'PATCH',
            body: JSON.stringify({
                "Title": contact.Title
            }),
            headers: headers
        })
```

## Using This Repository

To use this repository, simply follow the instructions in the [`salesforce-integration-service`](https://gitlab.com/johnjvester/salesforce-integration-service)  repository, then issue the following command:

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000) and a screen similar to what is listed below will appear:

![Original Contact List](./OriginalContactList.png)

At this point, the `Title` column is allowed to be inline edited and submitted to the RESTful Spring Boot service using a PATCH command.  That 
PATCH command then issues the necessary command to update the source data in Salesforce.

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.